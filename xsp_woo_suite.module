<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

use Drupal\xsp_woo_suite\ValueList\DocumentTypeTaxonomy;
use Drupal\xsp_woo_suite\ValueList\InformationCategoryTaxonomy;
use Drupal\xsp_woo_suite\XspWooSuite;

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */
function xsp_woo_suite_is_enabled(): bool
{
  return Drupal::service('config.factory')->get(XspWooSuite::SETTINGS_KEY)->get('enabled') ?? FALSE;
}

/**
 * Function for retrieving all WOO document types in a `xs_lists` compliant format.
 *
 * @return array<string, array{labels: array{nl-NL: string}, parent: null|string}> The formatted WOO document types
 */
function xs_woo_suite_document_type_taxonomy_list(): array
{
  return DocumentTypeTaxonomy::TYPES;
}

/**
 * Function for retrieving all WOO information categories in a `xs_lists` compliant format.
 *
 * @return array<string, array{labels: array{nl-NL: string}}> The formatted WOO information categories
 */
function xs_woo_suite_information_category_taxonomy_list(): array
{
  return InformationCategoryTaxonomy::CATEGORIES;
}
