<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

include 'vendor/autoload.php';

$package = Composer\InstalledVersions::getRootPackage()['name'];
$rules   = include XpertSelect\Tools\ProjectType::DrupalPackage->phpCsFixerRuleFile();

$rules['header_comment']['header'] = trim('
This file is part of the ' . $package . ' package.

This source file is subject to the license that is
bundled with this source code in the LICENSE.md file.
');

$finder = PhpCsFixer\Finder::create()
  ->in([__DIR__])
  ->name('*.inc')
  ->name('*.install')
  ->name('*.module')
  ->name('*.profile')
  ->name('*.php')
  ->name('*.theme')
  ->append([__FILE__])
  ->ignoreDotFiles(FALSE)
  ->ignoreVCSIgnored(TRUE);

return (new PhpCsFixer\Config('XpertSelect/Drupal'))
  ->setIndent('  ')
  ->setLineEnding("\n")
  ->setRules($rules)
  ->setFinder($finder);
