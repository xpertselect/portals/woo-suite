/* globals jQuery, Drupal, drupalSettings */

(function ($, Drupal) {
    Drupal.behaviors.notifyUser = {
        attach: function () {
            "use strict";

            let dirty = false;
            let formSubmit = false;
            const forms = $("form");

            forms.on("change", () => {
                dirty = true;
            });
            forms.on("submit", () => {
                formSubmit = true;
            });

            window.addEventListener("beforeunload", (event) => {
                if (dirty && !formSubmit) {
                    event.preventDefault();

                    return Drupal.t("Changes you made may not be saved.");
                }
            });
        },
    };
})(jQuery, Drupal, drupalSettings);
