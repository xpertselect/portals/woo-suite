<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xsp_woo_suite;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\xsp_woo_suite\XspWooSuite;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\TestCase as BaseTestCase;

/**
 * @internal
 */
abstract class TestCase extends BaseTestCase
{
  /**
   * @param bool $wooEnabled The value that wil be stored in the mocked config
   */
  protected function getMockedConfigFactory(bool $wooEnabled = TRUE): ConfigFactoryInterface
  {
    return M::mock(ConfigFactoryInterface::class, function(MI $mock) use ($wooEnabled) {
      $config = M::mock(Config::class, function(MI $mock) {
        $mock->shouldReceive('get')
          ->with('schema_field_content_type')
          ->andReturn('type');
      });

      $mock->shouldReceive('get')
        ->with('xs_searchable_content.settings')
        ->andReturn($config);

      $config = M::mock(Config::class, function(MI $mock) use ($wooEnabled) {
        $mock->shouldReceive('get')
          ->with('enabled')
          ->andReturn($wooEnabled);
      });

      $mock->shouldReceive('get')
        ->with(XspWooSuite::SETTINGS_KEY)
        ->andReturn($config);
    });
  }
}
