<?php

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xsp_woo_suite\Unit\Access;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\xsp_woo_suite\Access\WOOEnabledAccessCheck;
use Drupal\xsp_woo_suite\XspWooSuite;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_document\TestCase;

/**
 * @internal
 */
final class WOOEnabledAccessCheckTest extends TestCase
{
  public function testInstanceCanBeCreated(): void
  {
    $configFactory = $this->getMockedConfigFactory();

    Assert::assertInstanceOf(
      WOOEnabledAccessCheck::class,
      WOOEnabledAccessCheck::create($configFactory)
    );
  }

  public function testAccessCheckReturnsAllowed(): void
  {
    $configFactory = $this->getMockedConfigFactory();

    $WOOEnabledAccessCheck = WOOEnabledAccessCheck::create($configFactory);

    Assert::assertTrue($WOOEnabledAccessCheck->access()->isAllowed());
  }

  public function testAccessCheckReturnsNotAllowed(): void
  {
    $configFactory = $this->getMockedConfigFactory(FALSE);

    $WOOEnabledAccessCheck = WOOEnabledAccessCheck::create($configFactory);

    Assert::assertFalse($WOOEnabledAccessCheck->access()->isAllowed());
  }

  /**
   * @param bool $wooEnabled The value that wil be stored in the mocked config
   */
  protected function getMockedConfigFactory(bool $wooEnabled = TRUE): ConfigFactoryInterface
  {
    return M::mock(ConfigFactoryInterface::class, function(MI $mock) use ($wooEnabled) {
      $config = M::mock(Config::class, function(MI $mock) use ($wooEnabled) {
        $mock->shouldReceive('get')
          ->with('enabled')
          ->andReturn($wooEnabled);
      });

      $mock->shouldReceive('get')
        ->with(XspWooSuite::SETTINGS_KEY)
        ->andReturn($config);
    });
  }
}
