<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xsp_woo_suite\ValueList;

/**
 * Class InformationCategoryTaxonomy.
 *
 * Contains a list of WOO information categories in a representation supported by `xs_lists`.
 */
final class InformationCategoryTaxonomy
{
  /**
   * WOO information categories.
   *
   * @var array<string, array{labels: array{nl-NL: string}}>
   *
   * @see https://standaarden.overheid.nl/diwoo/metadata/doc/0.9.1/diwoo-metadata-lijsten_xsd_Simple_Type_diwoo_informatiecategorielijst#informatiecategorielijst
   */
  public const CATEGORIES = [
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_3baef532' => [
      'labels' => [
        'nl-NL' => 'Woo-verzoeken en -besluiten',
      ],
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_99a836c7' => [
      'labels' => [
        'nl-NL' => 'adviezen',
      ],
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_3a248e3a' => [
      'labels' => [
        'nl-NL' => 'agenda’s en besluitenlijsten bestuurscolleges',
      ],
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_89ee6784' => [
      'labels' => [
        'nl-NL' => 'bereikbaarheidsgegevens',
      ],
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_46a81018' => [
      'labels' => [
        'nl-NL' => 'beschikkingen',
      ],
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_8c840238' => [
      'labels' => [
        'nl-NL' => 'bij vertegenwoordigende organen ingekomen stukken',
      ],
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_8fc2335c' => [
      'labels' => [
        'nl-NL' => 'convenanten',
      ],
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_816e508d' => [
      'labels' => [
        'nl-NL' => 'inspanningsverplichting art 3.1 Woo',
      ],
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_c6cd1213' => [
      'labels' => [
        'nl-NL' => 'jaarplannen en jaarverslagen',
      ],
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_a870c43d' => [
      'labels' => [
        'nl-NL' => 'klachtoordelen',
      ],
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_fdaee95e' => [
      'labels' => [
        'nl-NL' => 'onderzoeksrapporten',
      ],
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_759721e2' => [
      'labels' => [
        'nl-NL' => 'ontwerpen van wet- en regelgeving met adviesaanvraag',
      ],
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_40a05794' => [
      'labels' => [
        'nl-NL' => 'organisatie en werkwijze',
      ],
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_aab6bfc7' => [
      'labels' => [
        'nl-NL' => 'overige besluiten van algemene strekking',
      ],
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_cf268088' => [
      'labels' => [
        'nl-NL' => 'subsidieverplichtingen anders dan met beschikking',
      ],
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_c76862ab' => [
      'labels' => [
        'nl-NL' => 'vergaderstukken Staten-Generaal',
      ],
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_db4862c3' => [
      'labels' => [
        'nl-NL' => 'vergaderstukken decentrale overheden',
      ],
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_139c6280' => [
      'labels' => [
        'nl-NL' => 'wetten en algemeen verbindende voorschriften',
      ],
    ],
  ];
}
