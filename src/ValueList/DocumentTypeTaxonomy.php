<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xsp_woo_suite\ValueList;

/**
 * Class DocumentTypeTaxonomy.
 *
 * Contains a hierarchical list of WOO document types into a representation supported by `xs_lists`.
 */
final class DocumentTypeTaxonomy
{
  /**
   * WOO document types.
   *
   * @var array<string, array{labels: array{nl-NL: string}, parent: null|string}>
   *
   * @see https://standaarden.overheid.nl/diwoo/metadata/doc/0.9.1/diwoo-metadata-lijsten_xsd_Simple_Type_diwoo_documentsoortlijst
   */
  public const TYPES = [
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_7051a61f' => [
      'labels' => [
        'nl-NL' => 'Adviesdocument',
      ],
      'parent' => NULL,
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_d506b718' => [
      'labels' => [
        'nl-NL' => 'Advies',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_7051a61f',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_a40458df' => [
      'labels' => [
        'nl-NL' => 'Adviesaanvraag',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_7051a61f',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_0e425c23' => [
      'labels' => [
        'nl-NL' => 'Adviesvoorstel',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_7051a61f',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_10eb451d' => [
      'labels' => [
        'nl-NL' => 'Beleidsdocument',
      ],
      'parent' => NULL,
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_dfa0ff1f' => [
      'labels' => [
        'nl-NL' => 'Begroting',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_10eb451d',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_9376c730' => [
      'labels' => [
        'nl-NL' => 'Beleidsnota',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_10eb451d',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_2977c34f' => [
      'labels' => [
        'nl-NL' => 'Beslisnota',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_10eb451d',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_a6f44748' => [
      'labels' => [
        'nl-NL' => 'Jaarplan',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_10eb451d',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_3d782f30' => [
      'labels' => [
        'nl-NL' => 'Jaarverslag',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_10eb451d',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_3300f29a' => [
      'labels' => [
        'nl-NL' => 'Rapport',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_10eb451d',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_8b92eab4' => [
      'labels' => [
        'nl-NL' => 'Ambtsbericht',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_3300f29a',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_38ba44de' => [
      'labels' => [
        'nl-NL' => 'Evaluatierapport',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_3300f29a',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_4efe1293' => [
      'labels' => [
        'nl-NL' => 'Inspectierapport',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_3300f29a',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_6f49bf34' => [
      'labels' => [
        'nl-NL' => 'Onderzoeksrapport',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_3300f29a',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_99d3e284' => [
      'labels' => [
        'nl-NL' => 'Verantwoordingsrapport',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_3300f29a',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_cccba364' => [
      'labels' => [
        'nl-NL' => 'Voortgangsrapport',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_3300f29a',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_a1dae55d' => [
      'labels' => [
        'nl-NL' => 'Termijnagenda',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_10eb451d',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_93b0d113' => [
      'labels' => [
        'nl-NL' => 'Beschikking',
      ],
      'parent' => NULL,
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_c1956ef0' => [
      'labels' => [
        'nl-NL' => 'Aanwijzigingsbeschikking',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_93b0d113',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_acb44d77' => [
      'labels' => [
        'nl-NL' => 'Benoemingsbeschikking',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_93b0d113',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_1c7656ac' => [
      'labels' => [
        'nl-NL' => 'Beschikking tot handhaving',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_93b0d113',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_45be34e9' => [
      'labels' => [
        'nl-NL' => 'Beschikking tot opleggen boete',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_1c7656ac',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_a55b7649' => [
      'labels' => [
        'nl-NL' => 'Beschikking tot opleggen last onder bestuursdwang',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_1c7656ac',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_566a4430' => [
      'labels' => [
        'nl-NL' => 'Beschikking tot opleggen last onder dwangsom',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_1c7656ac',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_30e8b503' => [
      'labels' => [
        'nl-NL' => 'Concessie',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_93b0d113',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_2ab17960' => [
      'labels' => [
        'nl-NL' => 'Erkenningsbeschikking',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_93b0d113',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_2c0438f4' => [
      'labels' => [
        'nl-NL' => 'Instemmingsbeschikking',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_93b0d113',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_f7dc55d9' => [
      'labels' => [
        'nl-NL' => 'Ontheffingsbeschikking',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_93b0d113',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_aebfec50' => [
      'labels' => [
        'nl-NL' => 'Subsidiebeschikking',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_93b0d113',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_002fc258' => [
      'labels' => [
        'nl-NL' => 'Vergunning',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_93b0d113',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_75007d14' => [
      'labels' => [
        'nl-NL' => 'Communicatie-uiting',
      ],
      'parent' => NULL,
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_7652d853' => [
      'labels' => [
        'nl-NL' => 'Brief',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_75007d14',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_97f44ea5' => [
      'labels' => [
        'nl-NL' => 'Brochure',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_75007d14',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_c2f56984' => [
      'labels' => [
        'nl-NL' => 'Nieuwsbericht',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_75007d14',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_9ecc0007' => [
      'labels' => [
        'nl-NL' => 'Organisatiegegevens',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_75007d14',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_7eba29ad' => [
      'labels' => [
        'nl-NL' => 'Persbericht',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_75007d14',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_2aedadff' => [
      'labels' => [
        'nl-NL' => 'Toespraak',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_75007d14',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_386e74cb' => [
      'labels' => [
        'nl-NL' => 'Convenant',
      ],
      'parent' => NULL,
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_7ab92a02' => [
      'labels' => [
        'nl-NL' => 'Document van burger/bedrijf',
      ],
      'parent' => NULL,
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_06a67c95' => [
      'labels' => [
        'nl-NL' => 'Bezwaarschrift',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_7ab92a02',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_ef935990' => [
      'labels' => [
        'nl-NL' => 'Klacht',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_7ab92a02',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_d943ca24' => [
      'labels' => [
        'nl-NL' => 'Ontheffingsaanvraag',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_7ab92a02',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_5824891d' => [
      'labels' => [
        'nl-NL' => 'Subsidieaanvraag',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_7ab92a02',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_dad2a6ed' => [
      'labels' => [
        'nl-NL' => 'Vergunningaanvraag',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_7ab92a02',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_df2cb56e' => [
      'labels' => [
        'nl-NL' => 'Zienswijze',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_7ab92a02',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/k_f49814b2' => [
      'labels' => [
        'nl-NL' => 'Publicatieblad',
      ],
      'parent' => NULL,
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_81cc2eb5' => [
      'labels' => [
        'nl-NL' => 'Gemeenteblad',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/k_f49814b2',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/k_1e96f394' => [
      'labels' => [
        'nl-NL' => 'Regelgeving en formele besluitvorming',
      ],
      'parent' => NULL,
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_fbaa7e4b' => [
      'labels' => [
        'nl-NL' => 'Beleidsregel',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/k_1e96f394',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_465163c9' => [
      'labels' => [
        'nl-NL' => 'Besluit',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/k_1e96f394',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_5b1055aa' => [
      'labels' => [
        'nl-NL' => 'Plan',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/k_1e96f394',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_7f9dffa7' => [
      'labels' => [
        'nl-NL' => 'Vergaderstuk',
      ],
      'parent' => NULL,
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_f90465b3' => [
      'labels' => [
        'nl-NL' => 'Agenda',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_7f9dffa7',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_d4a4792f' => [
      'labels' => [
        'nl-NL' => 'Besluitenlijst',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_7f9dffa7',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_de27ae7a' => [
      'labels' => [
        'nl-NL' => 'Ingekomen stuk',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_7f9dffa7',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_42e406dd' => [
      'labels' => [
        'nl-NL' => 'Vergaderverslag',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/c_7f9dffa7',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/k_149a3964' => [
      'labels' => [
        'nl-NL' => 'Woo-procedure',
      ],
      'parent' => NULL,
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_4f50ca9c' => [
      'labels' => [
        'nl-NL' => 'Woo-beslissing',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/k_149a3964',
    ],
    'https://identifier.overheid.nl/tooi/def/thes/kern/c_1cf18f83' => [
      'labels' => [
        'nl-NL' => 'Woo-verzoek',
      ],
      'parent' => 'https://identifier.overheid.nl/tooi/def/thes/kern/k_149a3964',
    ],
  ];
}
