<?php

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xsp_woo_suite;

/**
 * Class XspWooSuite.
 *
 * Simple PHP class holding various constants used by the `xsp_woo_suite` Drupal module.
 */
final class XspWooSuite
{
  /**
   * The key identifying the `xsp_woo_suite` settings in Drupal.
   */
  public const SETTINGS_KEY = 'xsp_woo_suite.settings';
}
