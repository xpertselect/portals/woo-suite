<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xsp_woo_suite\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\xsp_woo_suite\XspWooSuite;

/**
 * Class WOOEnabledAccessCheck.
 *
 * Access check for woo routes.
 */
final class WOOEnabledAccessCheck implements AccessInterface
{
  /**
   * Creates a new WOOEnabledAccessCheck instance.
   *
   * @param ConfigFactoryInterface $configFactory The config factory to get the relevant config
   */
  public static function create(ConfigFactoryInterface $configFactory): self
  {
    return new self($configFactory->get(XspWooSuite::SETTINGS_KEY)->get('enabled') ?? FALSE);
  }

  /**
   * WOOEnabledAccessCheck constructor.
   *
   * @param bool $wooEnabled A boolean indicating whether woo is enabled
   */
  public function __construct(private readonly bool $wooEnabled)
  {
  }

  /**
   * Checks if the woo module is enabled.
   *
   * @return AccessResultInterface The result of the access check
   */
  public function access(): AccessResultInterface
  {
    return AccessResult::allowedIf($this->wooEnabled);
  }
}
