<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xsp_woo_suite\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\xsp_woo_suite\XspWooSuite;

/**
 * Class AdminSettingsForm.
 *
 * A Drupal configuration form for managing the settings of the `xsp_woo_suite` module.
 */
final class AdminSettingsForm extends ConfigFormBase
{
  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'xsp_woo_suite_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    $config = $this->config(XspWooSuite::SETTINGS_KEY);

    $form['enabled'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enable WOO feature'),
      '#default_value' => $config->get('enabled') === TRUE ? 1 : 0,
      '#description'   => $this->t('When the woo feature is enabled the users have access the woo content-types and the corresponding features.') .
        ' ' . $this->t('After saving this setting the entire Drupal cache will be regenerated.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $config = $this->config(XspWooSuite::SETTINGS_KEY);

    $oldValue = $config->get('enabled');

    $config->set('enabled', $form_state->getValue('enabled') === 1)->save();

    if ($config->get('enabled') !== $oldValue) {
      $user = User::load(1);
      if ($config->get('enabled') && !is_null($user)) {
        $user->addRole('woo_beheerder');
        $user->save();
      } elseif (!is_null($user)) {
        $user->removeRole('woo_beheerder');
        $user->save();
      }
      drupal_flush_all_caches();
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return [XspWooSuite::SETTINGS_KEY];
  }
}
