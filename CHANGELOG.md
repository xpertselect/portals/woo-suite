# Changelog

## Unreleased (`dev-main`)

### Added

### Changed

### Fixed

---

## Release 1.11.1

### Fixed

- Updated the translation of `Format` on the `document` detail page to align with the rest of the application.

---

## Release 1.11.0

### Added

- Add 2 fields to `document` which displays the current action and the transition date.

### Changed

- When creating a new `document` which from a `dossier` page prefill the `document` form with values from the connected `dossier`.
- When creating a new `dossier` prefill the `email` field with the default email from config.
- When creating a new `dossier` validate that the given `Identificatienummer bronsysteem` is unique.

---

## Release 1.10.3

### Fixed

- Corrected several service argument definitions in `xs_dossier.services.yml` and `xs_document.services.yml` which prevented these services from being initialized correctly.

---

## Release 1.10.2

### Fixed

- Fixed an issue where the access check of a `dossier` would fail if the access was checked without the dossier in the `routeMatch`.
- Fixed an issue where the access check of a `document` would fail if the access was checked without the dossier in the `routeMatch`.

---

## Release 1.10.1

### Fixed

- Display the `document` create buttons in the cog wheel if the user can create a `document`.
- Display the `dossier` create buttons in the cog wheel if the user can create a `dossier`.

---

## Release 1.10.0

### Changed

- Only display an unpublished `document` if the user has the `woo beheerder` role and is member of the organization.
- Users with the `woo_beheerder` role can manage `document`s when the user is maintainer of the organization.
- Only display an unpublished `dossier` if the user has the `woo beheerder` role and is member of the organization.
- Users with the `woo_beheerder` role can manage `dossier`s when the user is maintainer of the organization.

---

## Release 1.9.0

### Changed

- The information category taxonomy is now literally copy-pasted from [here](https://standaarden.overheid.nl/diwoo/metadata/doc/0.9.4/diwoo-metadata-lijsten_xsd_Simple_Type_diwoo_informatiecategorielijst#informatiecategorielijst).

---

## Release 1.8.3

### Added

- When a user leaves the `Dossier` and `Document` forms with unsaved changes, show a warning.
- Add a button to the `Dossier` managed actions with which you can add create a `Document` and add it to the `Dossier`.

---

## Release 1.8.2

### Added

- Added a description to the `title` field of the `Dossier` and `Document` content type.

---

## Release 1.8.1

### Fixed

- Only show the `email` field title of a `Dossier` if the field has an value.
- Preserve the `Dossier` query parameter while previewing a `Document`.

---

## Release 1.8.0

### Added

### Changed

- Move the title of the `Document` and `Dossier` nodes to the node content to make it consistent with a dataset.
- Remove the description title on the detail page of the `Document` and `Dossier` nodes.
- Altered the view of the `bestand` and `extern bestand` fields of a `Document`, to make them more consistent with the `documenten` field.
- When creating a document, if a query parameter is available with a link to a dossier, automatically add the document to that dossier.
- Add an extra submit button to the dossier forms, which redirects you to the document creation form after saving.
- Change the title of the `gerelateerde dossiers` and `documenten` to a link.

### Fixed

- When updating a 'Dossier', invalidate the cache tags of the connected 'Document' nodes to ensure that the data of the 'Dossier' is still up-to-date.

---

## Release 1.7.1

### Fixed

- When the woo suite is turned on give the `WOO beheerder` role to the admin user.

---

## Release 1.7.0

### Added

- Add `relation_dataset` and `relation_dossier` fields to Dossier detail page.

---

## Release 1.6.0

### Changed

- 'WOO Document' and 'WOO Dossier' translations and descriptions changed to 'Document' and 'Dossier'.
- If a 'Woo Document' is related to a 'Woo Dossier', it will now be displayed on the 'Woo Document' page.

---

## Release 1.5.0

### Changed

- When the soo suite is activated in the form, automatically add the `woo_beheerder` role to the admin user.

---

## Release 1.4.0

### Added

- Added the fields Status, Disclosure level, Theme, maintainer, email to WOO dossiers.
- Added the fields Disclosure level, Format to WOO documents.
- The new fields of WOO dossiers are now indexed.
- The new fields of WOO documents are now indexed.

---

## Release 1.3.0

### Added

- Added the WOO information category taxonomy list.
- Information category of WOO documents are now indexed.
- A new information category filter is added to the search page.

---

## Release 1.2.0

### Added

- Document type of WOO documents are now indexed.
- A new document type filter is added to the search page.
- A WOO document search result now displays its document type.

### Fixed

- Corrected a typo in the document type taxonomy list.

---

## Release 1.1.0

### Added

- Added the WOO document type taxonomy list.

---

## Release 1.0.0

### Fixed

- Updated from PHP 8.1 to PHP 8.2
- Updated Xpertselect Tools to version 1.2

---

## Release 0.7.3

### Fixed

- Replace the default tooltips with bootstrap tooltips.
- Show document and dossier add buttons if you are super admin or have the role `woo_beheerder`.
- On the search page, alter the markup of the result details to a datalist element.

---

## Release 0.7.2

### Fixed

- Render the back button on the node preview pages.

---

## Release 0.7.1

### Fixed

- On the `woo_document` detail page, add context to the translation of `Maintainer`.

---

## Release 0.7.0

### Fixed

- Fixed permission checks for editing and deleting actions for `document` and `dossier` types.
- Display the organisation of a `document` node type.

---

## Release 0.6.0

### Changed

- Added both `document` and `dossier` to the controls by listen to the `AdminControlsPreRenderEvent`.
- Improved permission checks for editing and deleting actions for `document` and `dossier` types.
- Altered the breadcrumbs for `document` and `dossier` types in the add form.
- The `xs_document` module adds the document types to the search profile when the `PreparingResultsPerThemeBlock` event is fired.
- The `xs_dossier` module adds the dossier types to the search profile when the `PreparingResultsPerThemeBlock` event is fired.
- Added translationMappers for `woo_document` and `woo_dossier` to correctly display the facet values.

---

## Release 0.5.0

### Changed

- Created a toggle on the `woo_document` form page for switching between a link to a file or an input to upload a file.
- Added a template for rendering a `woo_document` search result.
- Added a template for rendering a `woo_dossier` search result.
- The routes `/document`, `/documents`, `/dossier`, `/dossiers` are redirected to the search all page.

---

## Release 0.4.0

### Added

- The `xs_dossier` module has been added to the suite.
- Rename the `document` content type to `woo_document`.
- Added functionality for the `woo_dossier` content type.

---

## Release 0.3.0

### Added

- The `xs_document` module adds the document types to the search profile when the `PreparingSuggestQuery` event is fired.

---

## Release 0.2.0

### Added

- Implemented a setting to turn the entire woo suite on or off.
- Created the necessary services to index the node type `document` into the search engine

---

## Release 0.1.0

### Added

- Initial project setup

