<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_dossier\Unit\EventSubscriber;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\xs_dossier\Access\CreateDossierAccessCheck;
use Drupal\xs_dossier\EventSubscriber\IncludeDossiersWhenSearchingSubscriber;
use Drupal\xs_searchable_content\Event\PreparingSearchQuery;
use Drupal\xs_searchable_content\Event\PreparingSuggestQuery;
use Drupal\xs_searchable_content\Services\Search\SearchProfile;
use Drupal\xs_searchable_content\Services\Search\SuggestProfile;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_dossier\TestCase;

/**
 * @internal
 */
final class IncludeDossiersWhenSearchingSubscriberTest extends TestCase
{
  public function testSubscriberListensToEvents(): void
  {
    Assert::assertNotEmpty(IncludeDossiersWhenSearchingSubscriber::getSubscribedEvents());
  }

  public function testInstanceCanBeCreated(): void
  {
    $configFactory = $this->getMockedConfigFactory();
    $dossierAccess = $this->getDossierAccessCheck();

    Assert::assertInstanceOf(
      IncludeDossiersWhenSearchingSubscriber::class,
      IncludeDossiersWhenSearchingSubscriber::create($configFactory, $dossierAccess)
    );
  }

  public function testInstanceAddsTheNecessaryDataToSuggestProfileWhenWooIsEnabled(): void
  {
    $configFactory = $this->getMockedConfigFactory();
    $dossierAccess = $this->getDossierAccessCheck();
    $types         = xs_dossier_entity_types();

    $event                = M::mock(PreparingSuggestQuery::class);
    $event->searchProfile = new SuggestProfile('type');

    Assert::assertEmpty($event->searchProfile->getTypes());

    $subscriber = IncludeDossiersWhenSearchingSubscriber::create($configFactory, $dossierAccess);
    $subscriber->includeDossiersWhenRetrievingSuggestions($event);

    Assert::assertEquals($types, $event->searchProfile->getTypes());
  }

  public function testInstanceDoesntAddTheNecessaryDataToSuggestProfileWhenWooIsDisabled(): void
  {
    $configFactory = $this->getMockedConfigFactory(FALSE);
    $dossierAccess = $this->getDossierAccessCheck();

    $event                = M::mock(PreparingSuggestQuery::class);
    $event->searchProfile = new SuggestProfile('type');

    Assert::assertEmpty($event->searchProfile->getTypes());

    $subscriber = IncludeDossiersWhenSearchingSubscriber::create($configFactory, $dossierAccess);
    $subscriber->includeDossiersWhenRetrievingSuggestions($event);

    Assert::assertEmpty($event->searchProfile->getTypes());
  }

  public function testInstanceAddsTheNecessaryDataToSearchProfileWhenWooIsEnabled(): void
  {
    $configFactory = $this->getMockedConfigFactory();
    $dossierAccess = $this->getDossierAccessCheck();
    $types         = xs_dossier_entity_types();

    $event                = M::mock(PreparingSearchQuery::class);
    $event->searchProfile = new SearchProfile('type');

    Assert::assertEmpty($event->searchProfile->getTypes());

    $subscriber = IncludeDossiersWhenSearchingSubscriber::create($configFactory, $dossierAccess);
    $subscriber->includeDossiersWhenRetrievingSearchResults($event);

    Assert::assertEquals($types, $event->searchProfile->getTypes());
  }

  public function testInstanceDoesntAddTheNecessaryDataToSearchProfileWhenWooIsDisabled(): void
  {
    $configFactory = $this->getMockedConfigFactory(FALSE);
    $dossierAccess = $this->getDossierAccessCheck();

    $event                = M::mock(PreparingSearchQuery::class);
    $event->searchProfile = new SearchProfile('type');

    Assert::assertEmpty($event->searchProfile->getTypes());

    $subscriber = IncludeDossiersWhenSearchingSubscriber::create($configFactory, $dossierAccess);
    $subscriber->includeDossiersWhenRetrievingSearchResults($event);

    Assert::assertEmpty($event->searchProfile->getTypes());
  }

  private function getDossierAccessCheck(): CreateDossierAccessCheck
  {
    return M::mock(CreateDossierAccessCheck::class, function(MI $mock) {
      $access = M::mock(AccessResultInterface::class, function(MI $mock) {
        $mock->shouldReceive('isForbidden')
          ->andReturn(FALSE);
      });

      $mock->shouldReceive('access')->andReturn($access);
    });
  }
}
