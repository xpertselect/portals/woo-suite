<?php

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_dossier\Unit\Access;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\NodeInterface;
use Drupal\xs_dossier\Access\ViewDossierAccessCheck;
use Drupal\xs_membership\MembershipService;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_dossier\TestCase;

/**
 * @internal
 */
final class ViewDossierAccessCheckTest extends TestCase
{
  public function testInstanceCanBeCreated(): void
  {
    Assert::assertInstanceOf(
      ViewDossierAccessCheck::class,
      $this->createViewDossierAccessCheck()
    );
  }

  public function testAccessCheckPublishedReturnsAllowed(): void
  {
    $viewDossierAccessCheck = $this->createViewDossierAccessCheck();
    $dossier                = $this->createDossier(TRUE);

    Assert::assertTrue($viewDossierAccessCheck->access($dossier)->isNeutral());
  }

  public function testAccessCheckReturnsAllowed(): void
  {
    $viewDossierAccessCheck = $this->createViewDossierAccessCheck();
    $dossier                = $this->createDossier();

    Assert::assertTrue($viewDossierAccessCheck->access($dossier)->isNeutral());
  }

  public function testAccessCheckNoMembershipsReturnsNotAllowed(): void
  {
    $viewDossierAccessCheck = $this->createViewDossierAccessCheck(FALSE);
    $dossier                = $this->createDossier();

    Assert::assertTrue($viewDossierAccessCheck->access($dossier)->isForbidden());
  }

  public function testAccessCheckIsNoUserRoleReturnsNotAllowed(): void
  {
    $viewDossierAccessCheck = $this->createViewDossierAccessCheck(TRUE, FALSE);
    $dossier                = $this->createDossier();

    Assert::assertTrue($viewDossierAccessCheck->access($dossier)->isForbidden());
  }

  public function testAccessCheckAllDisabledReturnsNotAllowed(): void
  {
    $viewDossierAccessCheck = $this->createViewDossierAccessCheck(FALSE, FALSE);
    $dossier                = $this->createDossier();

    Assert::assertTrue($viewDossierAccessCheck->access($dossier)->isForbidden());
  }

  protected function createViewDossierAccessCheck(bool $membershipAccess = TRUE, bool $userHasRole = TRUE): ViewDossierAccessCheck
  {
    return new ViewDossierAccessCheck(
      M::mock(MembershipService::class, function(MI $mock) use ($membershipAccess) {
        $mock->shouldReceive('getMembershipsForUser')
          ->andReturn($membershipAccess ? ['accessAllowed' => 'accessAllowed'] : []);
      }),
      M::mock(AccountProxyInterface::class, function(MI $mock) use ($userHasRole) {
        $mock->shouldReceive('getRoles')->andReturn($userHasRole ? ['woo_beheerder'] : []);
      })
    );
  }

  protected function createDossier(bool $isPublished = FALSE): NodeInterface
  {
    return M::mock(NodeInterface::class, function(MI $mock) use ($isPublished) {
    $fieldItemListInterface = M::mock(FieldItemList::class, function(MI $mock) {
      $mock->shouldReceive('getValue')->andReturn([0 => ['value' => 'accessAllowed']]);
    });

    $mock->shouldReceive('get')->with('field_bronhouder')
      ->andReturn($fieldItemListInterface);

    $mock->shouldReceive('isPublished')->andReturn($isPublished);
  });
  }
}
