<?php

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_dossier\Unit\Access;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\NodeInterface;
use Drupal\xs_dossier\Access\ManageDossierAccessCheck;
use Drupal\xs_membership\MembershipService;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_dossier\TestCase;

/**
 * @internal
 */
final class ManageDossierAccessCheckTest extends TestCase
{
  public function testInstanceCanBeCreated(): void
  {
    Assert::assertInstanceOf(
      ManageDossierAccessCheck::class,
      $this->createManageDossierAccessCheck()
    );
  }

  public function testAccessCheckReturnsAllowed(): void
  {
    $manageDossierAccessCheck = $this->createManageDossierAccessCheck();
    $dossier                  = $this->createDossier();

    Assert::assertTrue($manageDossierAccessCheck->access($dossier)->isNeutral());
  }

  public function testAccessCheckNoMembershipsReturnsNotAllowed(): void
  {
    $manageDossierAccessCheck = $this->createManageDossierAccessCheck(FALSE);
    $dossier                  = $this->createDossier();

    Assert::assertTrue($manageDossierAccessCheck->access($dossier)->isForbidden());
  }

  public function testAccessCheckIsNoUserRoleReturnsNotAllowed(): void
  {
    $manageDossierAccessCheck = $this->createManageDossierAccessCheck(TRUE, FALSE);
    $dossier                  = $this->createDossier();

    Assert::assertTrue($manageDossierAccessCheck->access($dossier)->isForbidden());
  }

  public function testAccessCheckAllDisabledReturnsNotAllowed(): void
  {
    $manageDossierAccessCheck = $this->createManageDossierAccessCheck(FALSE, FALSE);
    $dossier                  = $this->createDossier();

    Assert::assertTrue($manageDossierAccessCheck->access($dossier)->isForbidden());
  }

  protected function createManageDossierAccessCheck(bool $membershipAccess = TRUE, bool $userHasRole = TRUE): ManageDossierAccessCheck
  {
    return new ManageDossierAccessCheck(
      M::mock(MembershipService::class, function(MI $mock) use ($membershipAccess) {
        $mock->shouldReceive('getMembershipsForUser')
          ->andReturn($membershipAccess ? ['accessAllowed' => 'accessAllowed'] : []);
      }),
      M::mock(AccountProxyInterface::class, function(MI $mock) use ($userHasRole) {
        $mock->shouldReceive('getRoles')->andReturn($userHasRole ? ['woo_beheerder'] : []);
      })
    );
  }

  protected function createDossier(): NodeInterface
  {
      return M::mock(NodeInterface::class, function(MI $mock) {
        $fieldItemListInterface = M::mock(FieldItemList::class, function(MI $mock) {
          $mock->shouldReceive('getValue')->andReturn([0 => ['value' => 'accessAllowed']]);
        });

        $mock->shouldReceive('get')->with('field_bronhouder')
          ->andReturn($fieldItemListInterface);
      });
  }
}
