<?php

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_dossier\Unit\Access;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\xs_dossier\Access\CreateDossierAccessCheck;
use Drupal\xs_membership\MembershipService;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_dossier\TestCase;

/**
 * @internal
 */
final class CreateDossierAccessCheckTest extends TestCase
{
  public function testInstanceCanBeCreated(): void
  {
    Assert::assertInstanceOf(
      CreateDossierAccessCheck::class,
      $this->createCreateDossierAccessCheck()
    );
  }

  public function testAccessCheckReturnsAllowed(): void
  {
    $createDossierAccessCheck = $this->createCreateDossierAccessCheck();

    Assert::assertTrue($createDossierAccessCheck->access()->isNeutral());
  }

  public function testAccessCheckNoMembershipsReturnsNotAllowed(): void
  {
    $createDossierAccessCheck = $this->createCreateDossierAccessCheck(FALSE);

    Assert::assertTrue($createDossierAccessCheck->access()->isForbidden());
  }

  public function testAccessCheckIsNoUserRoleReturnsAllowed(): void
  {
    $createDossierAccessCheck = $this->createCreateDossierAccessCheck(TRUE, FALSE);

    Assert::assertTrue($createDossierAccessCheck->access()->isForbidden());
  }

  public function testAccessCheckBothDisabledReturnsNotAllowed(): void
  {
    $createDossierAccessCheck = $this->createCreateDossierAccessCheck(FALSE, FALSE);

    Assert::assertTrue($createDossierAccessCheck->access()->isForbidden());
  }

  protected function createCreateDossierAccessCheck(bool $membershipAccess = TRUE, bool $userHasRole = TRUE): CreateDossierAccessCheck
  {
    return new CreateDossierAccessCheck(
      M::mock(MembershipService::class, function(MI $mock) use ($membershipAccess) {
        $mock->shouldReceive('getMembershipsForUser')
          ->andReturn($membershipAccess ? ['accessAllowed'] : []);
      }),
      M::mock(AccountProxyInterface::class, function(MI $mock) use ($userHasRole) {
        $mock->shouldReceive('getRoles')->andReturn($userHasRole ? ['woo_beheerder'] : []);
      })
    );
  }
}
