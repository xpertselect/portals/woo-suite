<?php

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_dossier\Form\SearchableContent;

use Drupal\xs_document\Form\StandardContentIndexationFormCreationTrait;
use Drupal\xs_dossier\XsDossier;
use Drupal\xs_searchable_content\Form\BaseContentIndexationForm;
use RuntimeException;

/**
 * Class DossierIndexationForm.
 *
 * Form for indexing the dossier content type.
 */
class DossierIndexationForm extends BaseContentIndexationForm
{
  use StandardContentIndexationFormCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected function getContentTypeName(): string
  {
    return XsDossier::OVERARCHING_CONTENT_TYPE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getNodeToDocumentServiceName(): string
  {
    return 'xs_dossier.index.dossier_to_solr_document';
  }

  /**
   * {@inheritdoc}
   */
  protected function getContentIdentifiersToIndex(): array
  {
    $nodes = $this->nodeStorage->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', $this->getContentTypeName(), '=')
      ->execute();

    if (!is_array($nodes)) {
      throw new RuntimeException('Unexpected response from database');
    }

    return $nodes;
  }
}
