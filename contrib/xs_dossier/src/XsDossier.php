<?php

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_dossier;

/**
 * Class XsDossier.
 *
 * Simple PHP class holding various constants used by the `xs_dossier` Drupal module.
 */
final class XsDossier
{
  /**
   * The key identifying the `xs_dossier` settings in Drupal.
   */
  public const SETTINGS_KEY = 'xs_dossier.settings';

  /**
   * The log channel that should be used by this Drupal module.
   *
   * @var string
   */
  public const LOG_CHANNEL = 'xs_dossier';

  /**
   * Overarching type for all dossier content types.
   */
  public const OVERARCHING_CONTENT_TYPE = 'woo_dossier';
}
