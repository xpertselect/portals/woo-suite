<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_dossier\Services\TranslationMappers;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\xs_lists\Mapper\TranslationMapperInterface;

/**
 * Class DossierTranslationMapper.
 *
 * Adds a translation for "Dossier".
 */
class DossierTranslationMapper implements TranslationMapperInterface
{
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function mapping(): array
  {
    return [
      'woo_dossier' => [
        'labels' => [
          'nl-NL' => $this->t('Dossier')->render(),
          'en-US' => 'Dossier',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function name(): string
  {
    return 'ContentType';
  }
}
