<?php

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_dossier\Services\Converters;

use Drupal\Core\Entity\EntityInterface;
use Drupal\node\NodeInterface;
use Drupal\xs_dossier\XsDossier;
use Drupal\xs_searchable_content\NodeToDocumentServiceInterface;
use Drupal\xs_searchable_content\NodeValueExtractorTrait;
use Drupal\xs_solr\XsSolr;
use RuntimeException;

/**
 * Class DossierToSolrDocumentService.
 *
 * Implementation of DictionaryToDocumentServiceInterface, which defines how a
 * document dictionary should be mapped to the Solr search engine.
 */
final class DossierToSolrDocumentService implements NodeToDocumentServiceInterface
{
  use NodeValueExtractorTrait;

  /**
   * {@inheritdoc}
   */
  public function getSolrDocumentIdentifier(EntityInterface $node): string
  {
    return strval($node->id());
  }

  /**
   * {@inheritdoc}
   */
  public function mapNodeToSolrDocument(EntityInterface $node): array
  {
    if (!$node instanceof NodeInterface) {
      throw new RuntimeException(
        sprintf('Cannot map entity of type %s to a Solr document, only entities of type node are supported',
          $node->getEntityType()->getLabel()
        )
      );
    }

    return $this->getStoredFields($node);
  }

  /**
   * Get the stored fields for indexing a dossier.
   *
   * @param NodeInterface $node The dossier to get the stored fields from
   *
   * @return array<string, mixed> The stored fields for indexing
   */
  private function getStoredFields(NodeInterface $node): array
  {
    return array_filter([
      'id'                   => $this->getSolrDocumentIdentifier($node),
      'type'                 => XsDossier::OVERARCHING_CONTENT_TYPE,
      'title'                => $node->getTitle(),
      'description'          => $this->getRichTextStringValue($node, 'field_beschrijving'),
      'information_category' => $this->getFieldStringValue($node, 'field_informatiecategorie'),
      'status'               => $this->getFieldStringValue($node, 'field_status'),
      'organization'         => $this->getFieldStringValue($node, 'field_bronhouder'),
      'access_rights'        => $this->getFieldStringValue($node, 'field_openbaarheidsniveau'),
      'contact_email'        => $this->getFieldStringValue($node, 'field_e_mail'),
      'visibility'           => $node->isPublished() ? 'public' : 'private',
      'theme'                => $this->getFieldStringValueAsAnArray($node, 'field_thema', ', '),
      'created'              => date(XsSolr::DATETIME_FORMAT, $node->getCreatedTime()),
      'modified'             => date(XsSolr::DATETIME_FORMAT, $node->getChangedTime()),
      'tags'                 => $this->getFieldStringValueAsAnArray($node, 'field_trefwoorden', ', '),
    ]);
  }
}
