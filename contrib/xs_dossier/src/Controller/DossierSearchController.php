<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_dossier\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\xs_dossier\XsDossier;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class DossierSearchController.
 *
 * Provides search functionality for WOO Dossiers.
 */
final class DossierSearchController extends ControllerBase
{
  /**
   * Redirects a dossier query to the generic search page.
   *
   * @return RedirectResponse The redirect to the generic search page
   */
  public function searchDossiers(): RedirectResponse
  {
    return $this->redirect(
      'xs_searchable_content.web.search',
      [
        'query'          => '-',
        'filters'        => 'type:' . XsDossier::OVERARCHING_CONTENT_TYPE,
        'page_number'    => 1,
      ]
    );
  }
}
