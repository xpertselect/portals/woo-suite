<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_dossier\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\NodeInterface;
use Drupal\xs_membership\MembershipService;

/**
 * Class ManageDossierAccessCheck.
 *
 * Access check for managing dossiers.
 */
final readonly class ManageDossierAccessCheck implements AccessInterface
{
  /**
   * ManageDossierAccessCheck constructor.
   *
   * @param MembershipService     $membershipService The service for reasoning about the rights of a user in CKAN
   * @param AccountProxyInterface $currentUser       The current user
   */
  public function __construct(protected MembershipService $membershipService,
                              private AccountProxyInterface $currentUser)
  {
  }

  /**
   * Checks if a user may create dossiers. This is allowed under the following conditions.
   *
   * - The Drupal user has a "woo_beheerder" role
   * - The Drupal is part of the organisation of the dossier
   *
   * @param NodeInterface $dossier The dossier node to check the access for
   *
   * @return AccessResultInterface The result of the access check
   */
  public function access(NodeInterface $dossier): AccessResultInterface
  {
    if (!in_array('woo_beheerder', $this->currentUser->getRoles())) {
      return AccessResult::forbidden();
    }

    $memberships  = $this->membershipService->getMembershipsForUser(permission: 'admin', keyField: 'name');
    $organization = $dossier->get('field_bronhouder')->getValue();

    if (empty($organization)) {
      return AccessResult::forbidden(
        'Could not identify the organization of the dossier that must be managed'
      );
    }

    return AccessResult::forbiddenIf(
      !array_key_exists($organization[0]['value'] ?? '', $memberships)
    );
  }
}
