<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_dossier\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\xs_dossier\Access\CreateDossierAccessCheck;
use Drupal\xs_general\DrupalUrlUtilities;
use Drupal\xs_searchable_content\Event\PreparingSearchQuery;
use Drupal\xs_searchable_content\Event\PreparingSuggestQuery;
use Drupal\xs_solr\Solr\Search\Document;
use Drupal\xs_solr\Solr\Search\Highlighting;
use Drupal\xs_solr\Solr\SolrHighlightedFieldTrait;
use Drupal\xsp_woo_suite\XspWooSuite;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class IncludeDossiersWhenSearchingSubscriber.
 *
 * Listens to events pertaining to search profiles.
 */
final class IncludeDossiersWhenSearchingSubscriber implements EventSubscriberInterface
{
  use SolrHighlightedFieldTrait;
  use StringTranslationTrait;
  use DrupalUrlUtilities;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array
  {
    return [
      PreparingSuggestQuery::class => 'includeDossiersWhenRetrievingSuggestions',
      PreparingSearchQuery::class  => 'includeDossiersWhenRetrievingSearchResults',
    ];
  }

  /**
   * Creates a new IncludeDossiersWhenSearchingSubscriber instance.
   *
   * @param ConfigFactoryInterface   $configFactory            The config factory to get the relevant config
   * @param CreateDossierAccessCheck $createDossierAccessCheck The access check to determine if a user can create a dossier
   */
  public static function create(ConfigFactoryInterface $configFactory, CreateDossierAccessCheck $createDossierAccessCheck): self
  {
    return new self(
      $configFactory->get(XspWooSuite::SETTINGS_KEY)->get('enabled')                          ?? FALSE,
      $configFactory->get('xs_searchable_content.settings')->get('schema_field_content_type') ?? '',
      $createDossierAccessCheck,
    );
  }

  /**
   * IncludeDossiersWhenSearchingSubscriber constructor.
   *
   * @param bool                     $wooEnabled               A boolean indicating whether woo is enabled
   * @param string                   $contentTypeField         The field in which the content-type is stored in the index
   * @param CreateDossierAccessCheck $createDossierAccessCheck The access check to determine if a user can create a dossier
   */
  public function __construct(private readonly bool $wooEnabled,
                              private readonly string $contentTypeField,
                              private readonly CreateDossierAccessCheck $createDossierAccessCheck)
  {
  }

  /**
   * Alters the search profile of a search profile.
   *
   * @param PreparingSearchQuery $event The event holding the search profile
   */
  public function includeDossiersWhenRetrievingSearchResults(PreparingSearchQuery $event): void
  {
    if (!$this->wooEnabled) {
      return;
    }
    $types = xs_dossier_entity_types();

    $event->searchProfile->addTypes($types);

    $fieldList = [
      'id',
      'description',
      'title',
      'visibility',
      'created',
      $this->contentTypeField,
    ];

    $event->searchProfile->addFieldsToFieldList($fieldList);

    $event->searchProfile->addFacetMapping([
      'tag' => 'facet_tags',
    ]);

    $event->searchProfile->addFacetFields([
      'facet_tags',
    ]);

    $event->searchProfile->addAdditionalParameters([
      'f.facet_tags.facet.limit'   => 30,
      'f.facet_tags.facet.sort'    => 'count',
    ]);

    $converter = function(array $document, ?Highlighting $highlighting) {
      $document = new Document($document);
      $indexId  = $document->getStringField('id') ?? '';

      return [
        'render_template' => '@xs_dossier/search-results/result-view-type/result.html.twig',
        'index_id'        => $indexId,
        'is_private'      => $document->getStringField('visibility') === 'private',
        'title'           => $document->getStringField('title') ?? '',
        'link'            => $this->drupalUrlAsString(Url::fromRoute('entity.node.canonical', ['node' => $indexId])),
        'description'     => $this->getSolrHighlightedSnippetFromField($document, 'description', $indexId, $highlighting),
        'type_label'      => $this->t('Dossier')->render(),
        'type'            => $document->getStringField('type')    ?? '',
        'created'         => $document->getStringField('created') ?? '',
        'document'        => $document,
      ];
    };

    foreach ($types as $type) {
      $event->searchProfile->addResponseTypeConverter($type, $converter);
    }

    $this->addFilterForPrivateDossiers($event);
  }

  /**
   * Alters the search profile of a suggest search profile.
   *
   * @param PreparingSuggestQuery $event The event holding the search profile
   */
  public function includeDossiersWhenRetrievingSuggestions(PreparingSuggestQuery $event): void
  {
    if (!$this->wooEnabled) {
      return;
    }
    $types = xs_dossier_entity_types();

    $event->searchProfile->addTypes($types);

    foreach ($types as $type) {
      $event->searchProfile->addResponseTypeConverter($type, xs_dossier_convert_solr_document_to_suggestion());
    }

    $event->searchProfile->addFieldsToFieldList([
      'id',
      'machine_name',
      'title',
      'visibility',
      $this->contentTypeField,
    ]);

    $this->addFilterForPrivateDossiers($event);
  }

  private function addFilterForPrivateDossiers(PreparingSearchQuery|PreparingSuggestQuery $event): void
  {
    if ($this->createDossierAccessCheck->access()->isForbidden()) {
      $event->searchProfile->addFilters([sprintf('-(visibility:"private" AND (type:("%s")))', implode('" OR "', xs_dossier_entity_types()))]);
    }
  }
}
