<?php

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_document;

/**
 * Class XsDocument.
 *
 * Simple PHP class holding various constants used by the `xs_document` Drupal module.
 */
final class XsDocument
{
  /**
   * The key identifying the `xs_document` settings in Drupal.
   */
  public const SETTINGS_KEY = 'xs_document.settings';

  /**
   * The log channel that should be used by this Drupal module.
   *
   * @var string
   */
  public const LOG_CHANNEL = 'xs_document';

  /**
   * Overarching type for all document content types.
   */
  public const OVERARCHING_CONTENT_TYPE = 'woo_document';
}
