<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_document\Services\TranslationMappers;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\xs_lists\Mapper\TranslationMapperInterface;

/**
 * Class DocumentTranslationMapper.
 *
 * Adds a translation for "Document".
 */
class DocumentTranslationMapper implements TranslationMapperInterface
{
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function mapping(): array
  {
    return [
      'woo_document' => [
        'labels' => [
          'nl-NL' => $this->t('Document')->render(),
          'en-US' => 'Document',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function name(): string
  {
    return 'ContentType';
  }
}
