<?php

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_document\Form;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeStorageInterface;
use Drupal\xs_searchable_content\NodeIndexationService;
use Drupal\xs_searchable_content\XsSearchableContent;
use RuntimeException;
use Symfony\Component\DependencyInjection\ContainerInterface;

trait StandardContentIndexationFormCreationTrait
{
  /**
   * Instantiates a new instance of this class.
   *
   * @param ContainerInterface $container the service container this instance should use
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var NodeIndexationService $nodeIndexationService */
    $nodeIndexationService = $container->get('xs_searchable_content.node_indexation_service');

    /** @var EntityTypeManagerInterface $entityTypeManager */
    $entityTypeManager = $container->get('entity_type.manager');

    try {
      /** @var NodeStorageInterface $nodeStorage */
      $nodeStorage = $entityTypeManager->getStorage('node');
    } catch (PluginNotFoundException|InvalidPluginDefinitionException $e) {
      throw new RuntimeException('Unable to get node storage', previous: $e);
    }

    /** @var ConfigFactoryInterface $configFactory */
    $configFactory = $container->get('config.factory');

    $batchSize = $configFactory->get('xs_searchable_content.settings')->get('index_batch_size');

    return new self($nodeIndexationService, $nodeStorage, intval($batchSize) ?: XsSearchableContent::DEFAULT_INDEX_BATCH_SIZE);
  }
}
