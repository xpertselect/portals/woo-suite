<?php

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_document\Form\SearchableContent;

use Drupal\xs_document\Form\StandardContentIndexationFormCreationTrait;
use Drupal\xs_document\XsDocument;
use Drupal\xs_searchable_content\Form\BaseContentIndexationForm;
use RuntimeException;

class DocumentIndexationForm extends BaseContentIndexationForm
{
  use StandardContentIndexationFormCreationTrait;

  protected function getContentTypeName(): string
  {
    return XsDocument::OVERARCHING_CONTENT_TYPE;
  }

  protected function getNodeToDocumentServiceName(): string
  {
    return 'xs_document.index.document_to_solr_document';
  }

  /**
   * {@inheritdoc}
   */
  protected function getContentIdentifiersToIndex(): array
  {
    $nodes = $this->nodeStorage->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', $this->getContentTypeName(), '=')
      ->execute();

    if (!is_array($nodes)) {
      throw new RuntimeException('Unexpected response from database');
    }

    return $nodes;
  }
}
