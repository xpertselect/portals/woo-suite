<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_document\Breadcrumb;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\NodeType;
use Drupal\xs_document\XsDocument;

/**
 * Class DocumentFormBreadcrumbBuilder.
 *
 * Provides the breadcrumb for the webpage form pages.
 */
class DocumentFormBreadcrumbBuilder implements BreadcrumbBuilderInterface
{
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match): bool
  {
    $routesToMatch = ['node.add'];
    $nodeType      = $route_match->getParameter('node_type');

    if ($nodeType instanceof NodeType) {
      $nodeType = $nodeType->id();
    }

    return NULL !== $nodeType
      && in_array($nodeType, xs_document_entity_types())
      && in_array($route_match->getRouteName(), $routesToMatch);
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match): Breadcrumb
  {
    $breadcrumbs = new Breadcrumb();
    $breadcrumbs->addCacheContexts(['route']);

    $breadcrumbs->addLink(Link::createFromRoute(
      $this->t('Home'),
      '<front>'
    ));

    $nodeType = $route_match->getParameter('node_type');
    $nodeType = is_string($nodeType) ? $nodeType : $nodeType->id();

    $breadcrumbs->addLink(Link::createFromRoute(
      $this->t('Documents'),
      'xs_searchable_content.web.search',
      [
        'query'       => '-',
        'filters'     => 'type:' . XsDocument::OVERARCHING_CONTENT_TYPE,
        'page_number' => 1,
      ]
    ));

    $breadcrumbs->addLink(Link::createFromRoute(
      $this->t('Add'),
      'node.add',
      ['node_type' => $nodeType]
    ));

    return $breadcrumbs;
  }
}
