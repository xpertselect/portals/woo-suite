<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_document\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\xs_document\Access\CreateDocumentAccessCheck;
use Drupal\xs_general\DrupalUrlUtilities;
use Drupal\xs_searchable_content\Event\PreparingResultsPerThemeBlock;
use Drupal\xs_searchable_content\Event\PreparingSearchQuery;
use Drupal\xs_searchable_content\Event\PreparingSuggestQuery;
use Drupal\xs_solr\Solr\Search\Document;
use Drupal\xs_solr\Solr\Search\Highlighting;
use Drupal\xs_solr\Solr\SolrHighlightedFieldTrait;
use Drupal\xsp_woo_suite\XspWooSuite;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class IncludeDocumentsWhenSearchingSubscriber.
 *
 * Listens to events pertaining to search profiles.
 */
final class IncludeDocumentsWhenSearchingSubscriber implements EventSubscriberInterface
{
  use SolrHighlightedFieldTrait;
  use StringTranslationTrait;
  use DrupalUrlUtilities;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array
  {
    return [
      PreparingSuggestQuery::class         => 'includeDocumentsWhenRetrievingSuggestions',
      PreparingSearchQuery::class          => 'includeDocumentsWhenRetrievingSearchResults',
      PreparingResultsPerThemeBlock::class => 'includeDocumentsForResultsPerThemeBlock',
    ];
  }

  /**
   * Creates a new IncludeDocumentsWhenSearchingSubscriber instance.
   *
   * @param ConfigFactoryInterface    $configFactory             The config factory to get the relevant config
   * @param CreateDocumentAccessCheck $createDocumentAccessCheck The access check to determine if a user can create a document
   */
  public static function create(ConfigFactoryInterface $configFactory, CreateDocumentAccessCheck $createDocumentAccessCheck): self
  {
    return new self(
      $configFactory->get(XspWooSuite::SETTINGS_KEY)->get('enabled')                          ?? FALSE,
      $configFactory->get('xs_searchable_content.settings')->get('schema_field_content_type') ?? '',
        $createDocumentAccessCheck
    );
  }

  /**
   * IncludeDocumentsWhenSearchingSubscriber constructor.
   *
   * @param bool                      $wooEnabled                A boolean indicating whether woo is enabled
   * @param string                    $contentTypeField          The field in which the content-type is stored in the index
   * @param CreateDocumentAccessCheck $createDocumentAccessCheck The access check to determine if a user can create a document
   */
  public function __construct(private readonly bool $wooEnabled,
                              private readonly string $contentTypeField,
                              private readonly CreateDocumentAccessCheck $createDocumentAccessCheck)
  {
  }

  /**
   * Alters the search profile of a search profile.
   *
   * @param PreparingSearchQuery $event The event holding the search profile
   */
  public function includeDocumentsWhenRetrievingSearchResults(PreparingSearchQuery $event): void
  {
    if (!$this->wooEnabled) {
      return;
    }
    $types = xs_document_entity_types();

    $event->searchProfile->addTypes($types);

    $fieldList = [
      'id',
      'description',
      'title',
      'visibility',
      'created',
      'document_type',
      $this->contentTypeField,
    ];

    $event->searchProfile->addFieldsToFieldList($fieldList);

    $event->searchProfile->addFacetMapping([
      'tag'                  => 'facet_tags',
      'theme'                => 'facet_theme',
      'document-type'        => 'facet_document_type',
      'information-category' => 'facet_information_category',
    ]);

    $event->searchProfile->addFacetFields([
      'facet_theme',
      'facet_tags',
      'facet_document_type',
      'facet_information_category',
    ]);

    $event->searchProfile->addAdditionalParameters([
      'f.facet_theme.facet.limit'                => -1,
      'f.facet_theme.facet.sort'                 => 'index',
      'f.facet_tags.facet.limit'                 => 30,
      'f.facet_tags.facet.sort'                  => 'count',
      'f.facet_document_type.facet.limit'        => -1,
      'f.facet_document_type.facet.sort'         => 'count',
      'f.facet_information_category.facet.limit' => -1,
      'f.facet_information_category.facet.sort'  => 'count',
    ]);

    $converter = function(array $document, ?Highlighting $highlighting) {
      $document = new Document($document);
      $indexId  = $document->getStringField('id') ?? '';

      return [
        'render_template' => '@xs_document/search-results/result-view-type/result.html.twig',
        'index_id'        => $indexId,
        'is_private'      => $document->getStringField('visibility') === 'private',
        'title'           => $document->getStringField('title') ?? '',
        'link'            => $this->drupalUrlAsString(Url::fromRoute('entity.node.canonical', ['node' => $indexId])),
        'description'     => $this->getSolrHighlightedSnippetFromField($document, 'description', $indexId, $highlighting),
        'type_label'      => $this->t('Document')->render(),
        'type'            => $document->getStringField('type')          ?? '',
        'document_type'   => $document->getStringField('document_type') ?? '',
        'created'         => $document->getStringField('created')       ?? '',
        'document'        => $document,
      ];
    };

    foreach ($types as $type) {
      $event->searchProfile->addResponseTypeConverter($type, $converter);
    }

    $this->addFilterForPrivateDocuments($event);
  }

  /**
   * Alters the search profile of a suggest search profile.
   *
   * @param PreparingSuggestQuery $event The event holding the search profile
   */
  public function includeDocumentsWhenRetrievingSuggestions(PreparingSuggestQuery $event): void
  {
    if (!$this->wooEnabled) {
      return;
    }
    $types = xs_document_entity_types();

    $event->searchProfile->addTypes($types);

    foreach ($types as $type) {
      $event->searchProfile->addResponseTypeConverter($type, xs_document_convert_solr_document_to_suggestion());
    }

    $event->searchProfile->addFieldsToFieldList([
      'id',
      'machine_name',
      'title',
      'visibility',
      $this->contentTypeField,
    ]);

    $this->addFilterForPrivateDocuments($event);
  }

  /**
   * Alters the search profile of the theme block search profile.
   */
  public function includeDocumentsForResultsPerThemeBlock(PreparingResultsPerThemeBlock $event): void
  {
    if (!$this->wooEnabled) {
      return;
    }

    $event->searchProfile->addTypes(xs_document_entity_types());

    $event->searchProfile->addAdditionalParameters([
      'facet.field' => 'facet_theme',
    ]);

    $this->addFilterForPrivateDocuments($event);
  }

  /**
   * Add a filter to exclude private documents if the current user can not create a document.
   *
   * @param PreparingSearchQuery|PreparingSuggestQuery|PreparingResultsPerThemeBlock $event The event to add the filter to
   */
  private function addFilterForPrivateDocuments(PreparingSearchQuery|PreparingSuggestQuery|PreparingResultsPerThemeBlock $event): void
  {
    if ($this->createDocumentAccessCheck->access()->isForbidden()) {
      $event->searchProfile->addFilters([sprintf('-(visibility:"private" AND (type:("%s")))', implode('" OR "', xs_document_entity_types()))]);
    }
  }
}
