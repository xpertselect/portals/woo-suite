<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_document\EventSubscriber;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\xs_document\Access\CreateDocumentAccessCheck;
use Drupal\xs_document\XsDocument;
use Drupal\xs_general\DrupalUrlUtilities;
use Drupal\xs_general\Event\AdminControlsPreRenderEvent;
use Drupal\xsp_woo_suite\Access\WOOEnabledAccessCheck;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class AdminControlsSubscriber.
 *
 * Listens to events pertaining to the admin controls.
 */
final class AdminControlsSubscriber implements EventSubscriberInterface
{
  use DrupalUrlUtilities;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array
  {
    return [AdminControlsPreRenderEvent::class => 'adminControlsPreRender'];
  }

  /**
   * AdminControlsSubscriber constructor.
   *
   * @param CreateDocumentAccessCheck $createDocumentAccessCheck Check the permissions of the user if the user can create a document
   * @param WOOEnabledAccessCheck     $WOOEnabledAccessCheck     Check the permissions of the user if woo is enabled
   */
  public function __construct(private readonly CreateDocumentAccessCheck $createDocumentAccessCheck,
                              private readonly WOOEnabledAccessCheck $WOOEnabledAccessCheck)
  {
  }

  /**
   * Alters the admin control options depending on the rights of a user.
   *
   * @param AdminControlsPreRenderEvent $event The event holding the action list
   */
  public function adminControlsPreRender(AdminControlsPreRenderEvent $event): void
  {
    if ($this->WOOEnabledAccessCheck->access()->isAllowed() && !$this->createDocumentAccessCheck->access()->isForbidden()) {
      $url = Url::fromRoute('node.add', ['node_type' => xs_document_entity_types()[0]]);

      $event->adminControls[] = [
        'label'       => $this->t('Document')->render(),
        'description' => $this->t('Publish a new document on the catalog')->render(),
        'url'         => $this->drupalUrlAsString($url),
        'type'        => XsDocument::OVERARCHING_CONTENT_TYPE,
      ];
    }
  }
}
