<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_document\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\xs_document\XsDocument;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class DocumentSearchController.
 *
 * Provides search functionality for Documents.
 */
final class DocumentSearchController extends ControllerBase
{
  /**
   * Redirects a document query to the generic search page.
   *
   * @return RedirectResponse The redirect to the generic search page
   */
  public function searchDocuments(): RedirectResponse
  {
    return $this->redirect(
      'xs_searchable_content.web.search',
      [
        'query'          => '-',
        'filters'        => 'type:' . XsDocument::OVERARCHING_CONTENT_TYPE,
        'page_number'    => 1,
      ]
    );
  }
}
