<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_document\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\NodeInterface;
use Drupal\xs_membership\MembershipService;

/**
 * Class ViewDocumentAccessCheck.
 *
 * Access check for creating new datasets.
 */
final readonly class ViewDocumentAccessCheck implements AccessInterface
{
  /**
   * ViewDocumentAccessCheck constructor.
   *
   * @param MembershipService     $membershipService The service for reasoning about the rights of a user in CKAN
   * @param AccountProxyInterface $currentUser       The current user
   */
  public function __construct(protected MembershipService $membershipService,
                              private AccountProxyInterface $currentUser)
  {
  }

  /**
   * Checks if a user may create documents. This is allowed under the following conditions.
   *
   * - The Drupal user has a "woo_beheerder" role
   * - The Drupal is part of the organisation of the document
   *
   * @param NodeInterface $document The document node to check the access for
   *
   * @return AccessResultInterface The result of the access check
   */
  public function access(NodeInterface $document): AccessResultInterface
  {
    if ($document->isPublished()) {
      return AccessResult::neutral();
    }

    if (!in_array('woo_beheerder', $this->currentUser->getRoles())) {
      return AccessResult::forbidden();
    }

    $memberships  = $this->membershipService->getMembershipsForUser(keyField: 'name');
    $organization = $document->get('field_bronhouder')->getValue();

    if (empty($organization)) {
      return AccessResult::forbidden(
        'Could not identify the organization of the document that must be managed'
      );
    }

    return AccessResult::forbiddenIf(
      !array_key_exists($organization[0]['value'] ?? '', $memberships)
    );
  }
}
