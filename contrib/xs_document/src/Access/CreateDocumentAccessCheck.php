<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\xs_document\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\xs_membership\MembershipService;

/**
 * Class CreateDocumentAccessCheck.
 *
 * Access check for creating new documents.
 */
final class CreateDocumentAccessCheck implements AccessInterface
{
  /**
   * CreateDocumentAccessCheck constructor.
   *
   * @param MembershipService     $membershipService The service for reasoning about the rights of a user in CKAN
   * @param AccountProxyInterface $currentUser       The current user
   */
  public function __construct(private readonly MembershipService $membershipService,
                              private readonly AccountProxyInterface $currentUser)
  {
  }

  /**
   * Checks if a user may create documents. This is allowed under the following conditions.
   *
   * - The Drupal user has a corresponding CKAN user
   * - The Drupal user has a "woo_beheerder" role
   *
   * @return AccessResultInterface The result of the access check
   */
  public function access(): AccessResultInterface
  {
    if (!in_array('woo_beheerder', $this->currentUser->getRoles())) {
      return AccessResult::forbidden();
    }

    $memberships = $this->membershipService
      ->getMembershipsForUser(permission: 'create_dataset');

    return AccessResult::forbiddenIf(count($memberships) === 0);
  }
}
