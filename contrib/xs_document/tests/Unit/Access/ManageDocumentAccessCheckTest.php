<?php

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_document\Unit\Access;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\NodeInterface;
use Drupal\xs_document\Access\ManageDocumentAccessCheck;
use Drupal\xs_membership\MembershipService;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_document\TestCase;

/**
 * @internal
 */
final class ManageDocumentAccessCheckTest extends TestCase
{
  public function testInstanceCanBeCreated(): void
  {
    Assert::assertInstanceOf(
      ManageDocumentAccessCheck::class,
      $this->createManageDocumentAccessCheck()
    );
  }

  public function testAccessCheckReturnsAllowed(): void
  {
    $manageDocumentAccessCheck = $this->createManageDocumentAccessCheck();
    $document                  = $this->createDocument();

    Assert::assertTrue($manageDocumentAccessCheck->access($document)->isNeutral());
  }

  public function testAccessCheckNoMembershipsReturnsNotAllowed(): void
  {
    $manageDocumentAccessCheck = $this->createManageDocumentAccessCheck(FALSE);
    $document                  = $this->createDocument();

    Assert::assertTrue($manageDocumentAccessCheck->access($document)->isForbidden());
  }

  public function testAccessCheckIsNoUserRoleReturnsNotAllowed(): void
  {
    $manageDocumentAccessCheck = $this->createManageDocumentAccessCheck(TRUE, FALSE);
    $document                  = $this->createDocument();

    Assert::assertTrue($manageDocumentAccessCheck->access($document)->isForbidden());
  }

  public function testAccessCheckAllDisabledReturnsNotAllowed(): void
  {
    $manageDocumentAccessCheck = $this->createManageDocumentAccessCheck(FALSE, FALSE);
    $document                  = $this->createDocument();

    Assert::assertTrue($manageDocumentAccessCheck->access($document)->isForbidden());
  }

  protected function createManageDocumentAccessCheck(bool $membershipAccess = TRUE, bool $userHasRole = TRUE): ManageDocumentAccessCheck
  {
    return new ManageDocumentAccessCheck(
      M::mock(MembershipService::class, function(MI $mock) use ($membershipAccess) {
        $mock->shouldReceive('getMembershipsForUser')
          ->andReturn($membershipAccess ? ['accessAllowed' => 'accessAllowed'] : []);
      }),
      M::mock(AccountProxyInterface::class, function(MI $mock) use ($userHasRole) {
        $mock->shouldReceive('getRoles')->andReturn($userHasRole ? ['woo_beheerder'] : []);
      })
    );
  }

  protected function createDocument(): NodeInterface
  {
    return M::mock(NodeInterface::class, function(MI $mock) {
      $fieldItemListInterface = M::mock(FieldItemList::class, function(MI $mock) {
        $mock->shouldReceive('getValue')->andReturn([0 => ['value' => 'accessAllowed']]);
      });

      $mock->shouldReceive('get')->with('field_bronhouder')
        ->andReturn($fieldItemListInterface);
    });
  }
}
