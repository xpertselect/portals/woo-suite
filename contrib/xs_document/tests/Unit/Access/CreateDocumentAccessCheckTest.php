<?php

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_document\Unit\Access;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\xs_document\Access\CreateDocumentAccessCheck;
use Drupal\xs_membership\MembershipService;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_document\TestCase;

/**
 * @internal
 */
final class CreateDocumentAccessCheckTest extends TestCase
{
  public function testInstanceCanBeCreated(): void
  {
    Assert::assertInstanceOf(
      CreateDocumentAccessCheck::class,
      $this->createCreateDocumentAccessCheck()
    );
  }

  public function testAccessCheckReturnsAllowed(): void
  {
    $createDocumentAccessCheck = $this->createCreateDocumentAccessCheck();

    Assert::assertTrue($createDocumentAccessCheck->access()->isNeutral());
  }

  public function testAccessCheckNoMembershipsReturnsNotAllowed(): void
  {
    $createDocumentAccessCheck = $this->createCreateDocumentAccessCheck(FALSE);

    Assert::assertTrue($createDocumentAccessCheck->access()->isForbidden());
  }

  public function testAccessCheckIsNoUserRoleReturnsAllowed(): void
  {
    $createDocumentAccessCheck = $this->createCreateDocumentAccessCheck(TRUE, FALSE);

    Assert::assertTrue($createDocumentAccessCheck->access()->isForbidden());
  }

  public function testAccessCheckBothDisabledReturnsNotAllowed(): void
  {
    $createDocumentAccessCheck = $this->createCreateDocumentAccessCheck(FALSE, FALSE);

    Assert::assertTrue($createDocumentAccessCheck->access()->isForbidden());
  }

  protected function createCreateDocumentAccessCheck(bool $membershipAccess = TRUE, bool $userHasRole = TRUE): CreateDocumentAccessCheck
  {
    return new CreateDocumentAccessCheck(
      M::mock(MembershipService::class, function(MI $mock) use ($membershipAccess) {
        $mock->shouldReceive('getMembershipsForUser')
          ->andReturn($membershipAccess ? ['accessAllowed'] : []);
      }),
      M::mock(AccountProxyInterface::class, function(MI $mock) use ($userHasRole) {
        $mock->shouldReceive('getRoles')->andReturn($userHasRole ? ['woo_beheerder'] : []);
      })
    );
  }
}
