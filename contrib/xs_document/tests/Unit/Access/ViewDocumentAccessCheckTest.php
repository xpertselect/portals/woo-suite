<?php

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_document\Unit\Access;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\NodeInterface;
use Drupal\xs_document\Access\ViewDocumentAccessCheck;
use Drupal\xs_membership\MembershipService;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_document\TestCase;

/**
 * @internal
 */
final class ViewDocumentAccessCheckTest extends TestCase
{
  public function testInstanceCanBeCreated(): void
  {
    Assert::assertInstanceOf(
      ViewDocumentAccessCheck::class,
      $this->createViewDocumentAccessCheck()
    );
  }

  public function testAccessCheckPublishedReturnsAllowed(): void
  {
    $viewDocumentAccessCheck = $this->createViewDocumentAccessCheck();
    $document                = $this->createDocument(TRUE);

    Assert::assertTrue($viewDocumentAccessCheck->access($document)->isNeutral());
  }

  public function testAccessCheckReturnsAllowed(): void
  {
    $viewDocumentAccessCheck = $this->createViewDocumentAccessCheck();
    $document                = $this->createDocument();

    Assert::assertTrue($viewDocumentAccessCheck->access($document)->isNeutral());
  }

  public function testAccessCheckNoMembershipsReturnsNotAllowed(): void
  {
    $viewDocumentAccessCheck = $this->createViewDocumentAccessCheck(FALSE);
    $document                = $this->createDocument();

    Assert::assertTrue($viewDocumentAccessCheck->access($document)->isForbidden());
  }

  public function testAccessCheckAllDisabledReturnsNotAllowed(): void
  {
    $viewDocumentAccessCheck = $this->createViewDocumentAccessCheck(FALSE, FALSE);
    $document                = $this->createDocument();

    Assert::assertTrue($viewDocumentAccessCheck->access($document)->isForbidden());
  }

  public function testAccessCheckIsNoUserRoleReturnsNotAllowed(): void
  {
    $viewDocumentAccessCheck = $this->createViewDocumentAccessCheck(TRUE, FALSE);
    $document                = $this->createDocument();

    Assert::assertTrue($viewDocumentAccessCheck->access($document)->isForbidden());
  }

  protected function createViewDocumentAccessCheck(bool $membershipAccess = TRUE, bool $userHasRole = TRUE): ViewDocumentAccessCheck
  {
    return new ViewDocumentAccessCheck(
      M::mock(MembershipService::class, function(MI $mock) use ($membershipAccess) {
        $mock->shouldReceive('getMembershipsForUser')
          ->andReturn($membershipAccess ? ['accessAllowed' => 'accessAllowed'] : []);
      }),
      M::mock(AccountProxyInterface::class, function(MI $mock) use ($userHasRole) {
        $mock->shouldReceive('getRoles')->andReturn($userHasRole ? ['woo_beheerder'] : []);
      })
    );
  }

  protected function createDocument(bool $isPublished = FALSE): NodeInterface
  {
    return M::mock(NodeInterface::class, function(MI $mock) use ($isPublished) {
      $fieldItemListInterface = M::mock(FieldItemList::class, function(MI $mock) {
        $mock->shouldReceive('getValue')->andReturn([0 => ['value' => 'accessAllowed']]);
      });

      $mock->shouldReceive('get')->with('field_bronhouder')
        ->andReturn($fieldItemListInterface);

      $mock->shouldReceive('isPublished')->andReturn($isPublished);
    });
  }
}
