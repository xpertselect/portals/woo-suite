<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect-portals/xsp_woo_suite package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\xs_document\Unit\EventSubscriber;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\xs_document\Access\CreateDocumentAccessCheck;
use Drupal\xs_document\EventSubscriber\IncludeDocumentsWhenSearchingSubscriber;
use Drupal\xs_searchable_content\Event\PreparingResultsPerThemeBlock;
use Drupal\xs_searchable_content\Event\PreparingSearchQuery;
use Drupal\xs_searchable_content\Event\PreparingSuggestQuery;
use Drupal\xs_searchable_content\Services\Search\SearchProfile;
use Drupal\xs_searchable_content\Services\Search\SuggestProfile;
use Drupal\xs_searchable_content\Services\Search\ThemeBlockSearchProfile;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\xs_document\TestCase;

/**
 * @internal
 */
final class IncludeDocumentsWhenSearchingSubscriberTest extends TestCase
{
  public function testSubscriberListensToEvents(): void
  {
    Assert::assertNotEmpty(IncludeDocumentsWhenSearchingSubscriber::getSubscribedEvents());
  }

  public function testInstanceCanBeCreated(): void
  {
    $configFactory             = $this->getMockedConfigFactory();
    $createDocumentAccessCheck = $this->getDocumentAccessCheck();

    Assert::assertInstanceOf(
      IncludeDocumentsWhenSearchingSubscriber::class,
      IncludeDocumentsWhenSearchingSubscriber::create($configFactory, $createDocumentAccessCheck)
    );
  }

  public function testInstanceAddsTheNecessaryDataToSuggestProfileWhenWooIsEnabled(): void
  {
    $configFactory             = $this->getMockedConfigFactory();
    $createDocumentAccessCheck = $this->getDocumentAccessCheck();
    $types                     = xs_document_entity_types();

    $event                = M::mock(PreparingSuggestQuery::class);
    $event->searchProfile = new SuggestProfile('type');

    Assert::assertEmpty($event->searchProfile->getTypes());
    $subscriber = IncludeDocumentsWhenSearchingSubscriber::create($configFactory, $createDocumentAccessCheck);
    $subscriber->includeDocumentsWhenRetrievingSuggestions($event);

    Assert::assertEquals($types, $event->searchProfile->getTypes());
  }

  public function testInstanceDoesntAddTheNecessaryDataToSuggestProfileWhenWooIsDisabled(): void
  {
    $configFactory             = $this->getMockedConfigFactory(FALSE);
    $createDocumentAccessCheck = $this->getDocumentAccessCheck();

    $event                = M::mock(PreparingSuggestQuery::class);
    $event->searchProfile = new SuggestProfile('type');

    Assert::assertEmpty($event->searchProfile->getTypes());
    $subscriber = IncludeDocumentsWhenSearchingSubscriber::create($configFactory, $createDocumentAccessCheck);
    $subscriber->includeDocumentsWhenRetrievingSuggestions($event);

    Assert::assertEmpty($event->searchProfile->getTypes());
  }

  public function testInstanceAddsTheNecessaryDataToSearchProfileWhenWooIsEnabled(): void
  {
    $configFactory             = $this->getMockedConfigFactory();
    $createDocumentAccessCheck = $this->getDocumentAccessCheck();
    $types                     = xs_document_entity_types();

    $event                = M::mock(PreparingSearchQuery::class);
    $event->searchProfile = new SearchProfile('type');

    Assert::assertEmpty($event->searchProfile->getTypes());
    $subscriber = IncludeDocumentsWhenSearchingSubscriber::create($configFactory, $createDocumentAccessCheck);
    $subscriber->includeDocumentsWhenRetrievingSearchResults($event);

    Assert::assertEquals($types, $event->searchProfile->getTypes());
  }

  public function testInstanceDoesntAddTheNecessaryDataToSearchProfileWhenWooIsDisabled(): void
  {
    $configFactory             = $this->getMockedConfigFactory(FALSE);
    $createDocumentAccessCheck = $this->getDocumentAccessCheck();

    $event                = M::mock(PreparingSearchQuery::class);
    $event->searchProfile = new SearchProfile('type');

    Assert::assertEmpty($event->searchProfile->getTypes());
    $subscriber = IncludeDocumentsWhenSearchingSubscriber::create($configFactory, $createDocumentAccessCheck);
    $subscriber->includeDocumentsWhenRetrievingSearchResults($event);

    Assert::assertEmpty($event->searchProfile->getTypes());
  }

  public function testInstanceAddsTheNecessaryDataToPreparingResultsPerThemeBlockWhenWooIsEnabled(): void
  {
    $configFactory             = $this->getMockedConfigFactory();
    $createDocumentAccessCheck = $this->getDocumentAccessCheck();
    $types                     = xs_document_entity_types();

    $event                = M::mock(PreparingResultsPerThemeBlock::class);
    $event->searchProfile = new ThemeBlockSearchProfile();

    Assert::assertEmpty($event->searchProfile->getTypes());
    $subscriber = IncludeDocumentsWhenSearchingSubscriber::create($configFactory, $createDocumentAccessCheck);
    $subscriber->includeDocumentsForResultsPerThemeBlock($event);

    Assert::assertEquals($types, $event->searchProfile->getTypes());
  }

  public function testInstanceDoesntAddTheNecessaryDataToPreparingResultsPerThemeBlockWhenWooIsDisabled(): void
  {
    $configFactory             = $this->getMockedConfigFactory(FALSE);
    $createDocumentAccessCheck = $this->getDocumentAccessCheck();

    $event                = M::mock(PreparingResultsPerThemeBlock::class);
    $event->searchProfile = new ThemeBlockSearchProfile('type');

    Assert::assertEmpty($event->searchProfile->getTypes());
    $subscriber = IncludeDocumentsWhenSearchingSubscriber::create($configFactory, $createDocumentAccessCheck);
    $subscriber->includeDocumentsForResultsPerThemeBlock($event);

    Assert::assertEmpty($event->searchProfile->getTypes());
  }

  private function getDocumentAccessCheck(): CreateDocumentAccessCheck
  {
    return M::mock(CreateDocumentAccessCheck::class, function(MI $mock) {
      $access = M::mock(AccessResultInterface::class, function(MI $mock) {
        $mock->shouldReceive('isForbidden')
          ->andReturn(FALSE);
      });

      $mock->shouldReceive('access')->andReturn($access);
    });
  }
}
