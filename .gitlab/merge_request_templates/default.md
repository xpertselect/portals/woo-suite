### Project management

JIRA ticket: [PORTALS-XX](https://textinfo.atlassian.net/browse/PORTALS-XX)

### Definition of Done

- [ ] Any functional changes in the API are reflected in the Open API Specification
- [ ] New code is covered by unit and/or integration tests
- [ ] The changes adhere to the standards described in `CONTRIBUTING.md`
- [ ] The `CHANGELOG.md` file has been updated with the new changes
- [ ] The PHPDoc and/or Markdown documentation is updated (if applicable)
